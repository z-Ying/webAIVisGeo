const express = require('express')
const router = express.Router()
const userRouter = require('../RouterHandler/routerHandler_user')
router.post('/registry', userRouter.regUser)
router.post('/login', userRouter.login)
module.exports  = router