const express = require('express')
const router = express.Router()
const userRouter = require('../RouterHandler/routerHandler_target')
router.get('/targets', userRouter.targets)
router.get('/navigation', userRouter.navigation)
module.exports  = router