const db = require('../DB/database_target')
const navigation_db = require('../DB/database_navigation')

exports.targets = (req, res) => {
    const userinfo = req.body
    const sqlStr = 'select * from airports'
    db.query(sqlStr,  (err, results) => {
        if (err) {
            return res.err(err, 500)
        }
        res.send(results)
    })
}

exports.navigation = (req, res) => {
    const userinfo = req.body
    const sqlStr = 'select * from navigation'
    navigation_db.query(sqlStr,  (err, results) => {
        if (err) {
            return res.err(err, 500)
        }
        console.log(results);
        res.send(results)
    })
}


