const jwt = require('jsonwebtoken')
const db = require('../DB/database_user')
const bc = require('bcryptjs')
exports.regUser = (req, res) => {
    const userinfo = req.body
    const sqlStr = 'select * from user where username=?'
    db.query(sqlStr, userinfo.username, (err, results) => {
        if (err) {
            return res.err(err, 500)
        }
        if (results.length > 0) {
            return res.err('用户名被占用', 400)
        }
        userinfo.password = bc.hashSync(userinfo.password, 10)
        const sql = 'insert into user set ?'
        db.query(sql, { username: userinfo.username, password: userinfo.password }, (err, results) => {
            if (err) return res.send({ status: 1, message: err.message })
            if (results.affectedRows !== 1) return res.send({ status: 1, message: '注册失败' })
            res.success('注册成功！', 200)
        })
    })
}
const secre = 'zhaoying*~*#'

exports.login = (req, res) => {
    const userinfo = req.body
    const sql = 'select * from user where username=?' 
    db.query(sql, userinfo.username, (err, results) => {
        if (err) return res.err(err, 500)
        if (results.length !== 1) return res.err('登录失败', 400)
        const token = jwt.sign({ username: userinfo.username }, secre, { expiresIn: '1h' })
        console.log(token);
        res.send({
            status: 200,
            message: '登录成功！',
            token: token
        })
    })
}
