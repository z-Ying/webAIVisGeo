const express = require('express')
const bodyParser = require('body-parser');
const secretKey = 'zhaoying*~*#'

const app = express()
const expressWs = require('express-ws')(app) // 引入express-ws的WebSocket功能，并混入app，相当于为 app实例添加 .ws 方法
var socketRouter = require('./Router/routerHandler_ws');
app.use('/api',socketRouter) 
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
const expressJWT = require('express-jwt')
app.use(expressJWT({ secret: secretKey }).unless({ path: [/^\/api/] }))

const cors = require('cors')
app.use(cors())
app.use((req, res,next) => {
    res.err = (message, status =1) => {
        res.send({message, status})
    }
    res.success = (message, status =1) => {
        res.send({message, status})
    }
    next()
})
const userRouter = require('./Router/router_user')
app.use('/api', userRouter)

const targetsRouter = require('./Router/router_target')
app.use('/admin', targetsRouter)


app.use((err, req, res,next) => {
    if(err.name === 'UnauthorizedError') {
        return res.send({
            statues: 401,
            message: 'token过期'
        })
    }
    res.send({
        status: 500,
        message: '未知错误'
    })
})
app.listen(3001, () =>  {
    console.log('Server Start');
})