const { app, BrowserWindow } = require("electron")
function createWindow() {
  const win = new BrowserWindow({
    width: 1980,
    height: 900,
    minWidth: 800,
    minHeight: 800
  })
  win.loadURL("http://localhost:3000/")
}
app.whenReady().then(() => {
  createWindow()
  app.on("active", () => {
    if (BrowserWindow.getAllWindows().length === 0) {
      createWindow()
    }
  })
})
app.on("window-all-closed", () => {
  if (process.platform !== "darwin") {
    app.quit()
  }
})
