import React from 'react';
import EarthPage from '../Components/earthComponent'
import '../Styles/index.css'

class HomePage extends React.Component {
  hanldeToExit = () => {
    this.props.history.push('/');
  }
  componentDidMount() {
    // console.log(localstr);
  }
  render() {
    return (
      <div className="login-page">
        <div className="login-container">
          <h2>多源数据可视系统</h2>
          <div className='user-handler'>
            {<div>用户名：{localStorage.getItem('username')}</div> }
            { <span onClick={() => { this.hanldeToExit() }}>退出系统</span>}
          </div>
        </div>
        <div className='earth'>
          <EarthPage />
        </div>
      </div>
    )
  }
}

export default HomePage;

