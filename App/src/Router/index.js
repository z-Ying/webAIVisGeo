import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import HomePage from '../Page/index';
import Login from '../Page/loginIn'

const RouterList = () => {
  return (
    <Router>
      <Switch>
        <Route exact path="/" component={Login}></Route>
        <Route  exact path="/index" component={HomePage} />
      </Switch>
    </Router>
  );
};
export default RouterList;
