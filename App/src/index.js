import ReactDOM from 'react-dom/client';
import App from './App';
import Router from './Router/index'
import './index.css';

const rootElement = document.getElementById('root');
ReactDOM.createRoot(rootElement).render(
  <Router>
    <App /> 
  </Router>
);
