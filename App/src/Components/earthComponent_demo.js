import React from 'react';
import '../Styles/earthComponent.css'
import axios from 'axios';
// 待抽离
class EarthComponent extends React.Component {
  state = {
    showModal: false,
    targetData: [],
  };
  componentDidMount() {
    Cesium.Ion.defaultAccessToken = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiI4MmNkOWNhZC1hZjc4LTRjNDgtYTU2MC00ZjRkM2Q3NjQ4OTAiLCJpZCI6NzcxNDksImlhdCI6MTY1NzI2MTE4OH0.eE5ucAk-WLVL6V_X6C7ykrhDqRvQtldJq6JsEao3MBI';
    window.viewer = new Cesium.Viewer('cesiumContainer', {
      imageryProvider: Cesium.createWorldImagery(),
      baseLayerPicker: false,
      geocoder: false,
      homeButton: false,
      sceneModePicker: false,
      timeline: false,
      navigationHelpButton: false,
      animation: false,
    });

    // var socket = new WebSocket("ws://localhost:3001/api/mySocketUrl");
    // socket.onopen = function (e) {
    // }
    // socket.onmessage = function (e) {
    //   // 是否返回是二进制流 默认是Blob类型的
    //   console.log(e);
    // }
    this.handleAddTargetApi()
    this.handleSearchPlace()
  }

  componentDidUpdate() {

    this.handleAddTarget() // 此函数待优化，它将在每次数据更新后重新执行，

  }
  handleAddTargetApi() {
    return axios.get('http://127.0.0.1:3001/admin/targets', {
      headers: {
        'Authorization': `Bearer ${localStorage.getItem('token')}`
      }
    }).then(res => {
      console.log(res.data);
      this.setState({
        targetData: res.data
      })
    })
  }

  //  加载海量数据并做数据查询
  handleAddTarget() {
    const positions = [];
    let _this = this
    const colors = [];
    // Number.NEGATIVE_INFINITY 是js中的特殊值，表示负无穷大的数值。它是 Number 类的一个静态属性
    let west = Number.POSITIVE_INFINITY;
    let south = Number.POSITIVE_INFINITY;
    let east = Number.NEGATIVE_INFINITY;
    let north = Number.NEGATIVE_INFINITY;
    let TargetData = this.state.targetData

    var taskProcessor = new Cesium.TaskProcessor('../webWorker.js');
    console.log(taskProcessor, 'taskProcessor');
    // 定义任务函数
    function processPoints(points) {
      var result = [];
      for (var i = 0; i < points.length; i++) {
        // 处理每个点的逻辑
        var processedPoint = TargetData; // 根据需求进行处理
        result.push(processedPoint);
      }
      return result;
    }


    for (let i = 0; i < TargetData.length; ++i) {
      const lon = TargetData[i].longitude_deg;
      const lat = TargetData[i].latitude_deg;

      west = Math.min(west, lon);
      south = Math.min(south, lat);
      east = Math.max(east, lon);
      north = Math.max(north, lat);

      const position = Cesium.Cartesian3.fromDegrees(
        lon,
        lat,
        0
      );

      // 截至
      positions.push(position);
      const color = new Cesium.Color(
        Math.random(),
        Math.random(),
        Math.random(),
        1.0
      );
      colors.push(color);

      //   // 飞到目标范围
      handleFlyToTarget(west, south, east, north)
      function handleFlyToTarget(west, south, east, north) {
        var rectangle = Cesium.Rectangle.fromDegrees(west, south, east, north);
        viewer.camera.flyTo({
          destination: rectangle,
          duration: 2  // 动画持续时间（以秒为单位）
        });
      }
    }



    var billboardBatch = new Cesium.BillboardCollection();
    console.log(colors[2], 'colors[i]');
    // 加载五万个图片
    for (var i = 0; i < TargetData.length; i++) {
      const lon = TargetData[i].longitude_deg;
      const lat = TargetData[i].latitude_deg;
      var billboard = billboardBatch.add({
        position : Cesium.Cartesian3.fromDegrees(lon, lat, 0),
        image : require('../Public/Image/AirportImage/一级机场.png'),
        color:  Cesium.Color.fromCssColorString('#0ccd73'),
        scale: .4,
      });
    }

    // 将BillboardBatch添加到场景中
    viewer.scene.primitives.add(billboardBatch);


    // var billboardCollection = new Cesium.BillboardCollection();
    // viewer.scene.primitives.add(billboardCollection);

    // for (var i = 0; i < TargetData.length; i++) {
    //   const lon = TargetData[i].longitude_deg;
    //   const lat = TargetData[i].latitude_deg;
    //   var position = Cesium.Cartesian3.fromDegrees(lon, lat, 0);
    //   var image = require('../Public/Image/AirportImage/飞机场.png');
    //   var scale = 1.0;
    //   var colorMaterial = new Cesium.ColorMaterialProperty(Cesium.Color.fromRandom());
    //   billboardCollection.add({
    //     position: position,
    //     image: image,
    //     scale: scale,
    //     color: Cesium.Color.fromRandom(),
    //   });
    // }


    // // // 创建点集合
    // const primitive = new Cesium.PointPrimitiveCollection();
    // const primitiveList = []
    // // // 创建点并设置颜色
    // for (let i = 0; i < TargetData.length; ++i) {
    //   const primitiveElement = primitive.add({
    //     id: TargetData[i].VesselType,
    //     position: positions[i],
    //     color: colors[i],
    //     pixelSize: 15,
    //   });
    //   primitiveList.push(primitiveElement)
    // }
    // // // 添加点集合到场景
    // viewer.scene.primitives.add(primitive);


    // 数据查询
    var handler = new Cesium.ScreenSpaceEventHandler(viewer.scene.canvas);
    handler.setInputAction(function (click) {
      var pickedObject = viewer.scene.pick(click.position);
      if (Cesium.defined(pickedObject) && primitiveList.indexOf(pickedObject.primitive) !== -1) {
        var pickedId = pickedObject.id
        var pickedPoint = getPointFromPrimitive(pickedId);
        showPopup(pickedPoint);
        openModal()
      }
    }, Cesium.ScreenSpaceEventType.LEFT_CLICK);
    return

    // 获取点击的点
    function getPointFromPrimitive(primitiveId) {
      for (let i = 0; i < TargetData.length; i++) {
        if (TargetData[i].VesselType === primitiveId) {
          return TargetData[i]
        }
      }
    }
    // 显示弹窗
    function showPopup(point) {
      _this.setState({
        targetData: point
      });

    }
    function openModal() {
      _this.setState({
        showModal: true
      });
    };

    function closeModal() {
      _this.setState({
        showModal: false
      });
    };

  };

  // 搜索功能
  handleSearchPlace() {
    // 添加搜索地点功能
    new Cesium.Geocoder({
      container: 'cesiumSearchContainer',
      scene: viewer.scene,
    });
  }
  render() {
    const { showModal, targetData } = this.state;
    return (
      <div >
        <div id="cesiumSearchContainer"></div>
        <div id="cesiumContainer" style={{ width: '100%', height: '100%' }}></div>
        {showModal && (
          <div id="popup" >
            <div>
            </div>
            <ul>
              {Object.keys(targetData).map((key) => (
                <li key={key}>
                  <strong>{key}: </strong>
                  {targetData[key] || '暂无信息'}
                </li>
              ))}
            </ul>
          </div>
        )}
      </div>
    );
  }
}
export default EarthComponent;

