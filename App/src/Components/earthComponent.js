import React from 'react';
import '../Styles/earthComponent.css'
import axios from 'axios';
import { AddMassiveImages, AddMassivePoints } from '../Tools/TargetTools/addMassiveData';
import ChinaPlaceData from '../Public/localData/中华人民共和国.json'
// 待抽离
class EarthComponent extends React.Component {
  state = {
    showModal: false,
    targetData: {
      airportImageData: [],
      navigation: []

    }
  };
  componentDidMount() {
    Cesium.Ion.defaultAccessToken = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiI4MmNkOWNhZC1hZjc4LTRjNDgtYTU2MC00ZjRkM2Q3NjQ4OTAiLCJpZCI6NzcxNDksImlhdCI6MTY1NzI2MTE4OH0.eE5ucAk-WLVL6V_X6C7ykrhDqRvQtldJq6JsEao3MBI';
    window.viewer = new Cesium.Viewer('cesiumContainer', {
      imageryProvider: Cesium.createWorldImagery(),
      baseLayerPicker: false,
      geocoder: false,
      homeButton: false,
      sceneModePicker: false,
      timeline: false,
      navigationHelpButton: false,
      animation: false,
      
    });
    viewer.scene.skyBox = new Cesium.SkyBox({
      sources: {
        positiveX: Cesium.buildModuleUrl('Assets/Textures/SkyBox/tycho2t3_80_px.jpg'),
        negativeX: Cesium.buildModuleUrl('Assets/Textures/SkyBox/tycho2t3_80_mx.jpg'),
        positiveY: Cesium.buildModuleUrl('Assets/Textures/SkyBox/tycho2t3_80_py.jpg'),
        negativeY: Cesium.buildModuleUrl('Assets/Textures/SkyBox/tycho2t3_80_my.jpg'),
        positiveZ: Cesium.buildModuleUrl('Assets/Textures/SkyBox/tycho2t3_80_pz.jpg'),
        negativeZ: Cesium.buildModuleUrl('Assets/Textures/SkyBox/tycho2t3_80_mz.jpg')
      }
    });
    this.handleAddTargetApi()
    this.handleAddNavigationApi()
    this.handleSearchPlace()
    this.handleAddChinaData()
  }

  componentDidUpdate() {


  }
  handleAddTargetApi() {
    return axios.get('http://127.0.0.1:3001/admin/targets', {
      headers: {
        'Authorization': `Bearer ${localStorage.getItem('token')}`
      }
    }).then(res => {
      this.state.targetData.airportImageData = res.data
      // let c = targetData.airportImageData
      this.setState({
        airportImageData: this.state.targetData.airportImageData
      })
      this.handleAddTarget() // 此函数待优化，它将在每次数据更新后重新执行，

    })
  }

  handleAddNavigationApi() {
    axios.get('http://127.0.0.1:3001/admin/navigation', {
      headers: {
        'Authorization': `Bearer ${localStorage.getItem('token')}`
      }
    }).then(res => {
      this.state.targetData.navigation = res.data
      // let c = targetData.airportImageData
      this.setState({
        navigation: this.state.targetData.navigation
      })
    })
  }

  handleAddChinaData() {
    var promise = Cesium.GeoJsonDataSource.load(ChinaPlaceData);
    promise.then(function (dataSource) {
      viewer.dataSources.add(dataSource);
      var entities = dataSource.entities.values;
      for (var i = 0; i < entities.length; i++) {
        var entity = entities[i];
        entity.polygon.material = Cesium.Color.fromRandom({
          red: 1,
          maximumGreen: 1,
          maximumBlue: 1,
            alpha : 1.0
        });;
        entity.polygon.outline = true;
        const adCode = entity.properties.adcode._value;
        if( typeof adCode !== "string") {
              entity.polygon.extrudedHeight = adCode;
        } else {
          entity.polygon.extrudedHeight = Number(adCode.slice(0,-3));
        }
      }
    })
    // .catch(err) {
    //   throw new Error(`handleAddChinaData函数（加载Geojson）不可用！
    //   错误原因：${handleAddChinaData}`
    //   )
    // };
    
    // viewer.flyTo(promise);


    return
    // 创建 Cesium 的 GeoJsonDataSource
    var dataSource = new Cesium.GeoJsonDataSource();

    // 加载 GEOJSON 数据
    Cesium.GeoJsonDataSource.load(ChinaPlaceData).then(function (result) {
      viewer.dataSources.add(result);
      var entities = result.entities;
      entities.values.forEach(function (entity, index) {

        // console.log(entity.id, index);
        // console.log(ChinaPlaceData[entity.id]);

        // 打印经纬度信息
        console.log(entity);

        // entity.billboard.color = Cesium.Color.RED;
        // entity.billboard.outlineColor = Cesium.Color.RED;
        // entity.billboard.outlineWidth = 7.0;
        // // 随机生成一个高度值
        // var height = Math.random() * 1000;

        // 设置实体的高度属性
        // entity.position = Cesium.Cartesian3.fromDegrees(
        //   entity.position.longitude,
        //   entity.position.latitude,
        //   height
        // );
      });
      return
      // 将数据源添加到 Viewer 中

      // 获取已加载的实体集合

      // 对实体进行自定义操作

    })
  }

  //  加载海量数据并做数据查询
  handleAddTarget() {
    console.log(this.state.targetData, 'this.state.targetData');
    const airportImageData = this.state.targetData.airportImageData
    const navigationPointData = this.state.targetData.navigation
    const positions = [];
    let _this = this
    const colors = [];
    // Number.NEGATIVE_INFINITY 是js中的特殊值，表示负无穷大的数值。它是 Number 类的一个静态属性
    let west = Number.POSITIVE_INFINITY;
    let south = Number.POSITIVE_INFINITY;
    let east = Number.NEGATIVE_INFINITY;
    let north = Number.NEGATIVE_INFINITY;
    let TargetData = this.state.targetData
    new AddMassiveImages(airportImageData, '', "#ffffff").handlerAddImages()
    const primitiveList = new AddMassivePoints(navigationPointData).handlerAddPoints()

    return
    //数据查询
    var handler = new Cesium.ScreenSpaceEventHandler(viewer.scene.canvas);
    handler.setInputAction(function (click) {
      var pickedObject = viewer.scene.pick(click.position);
      if (Cesium.defined(pickedObject) && primitiveList.indexOf(pickedObject.primitive) !== -1) {
        var pickedId = pickedObject.id
        var pickedPoint = getPointFromPrimitive(pickedId);
        showPopup(pickedPoint);
        openModal()
      }
    }, Cesium.ScreenSpaceEventType.LEFT_CLICK);

    // 获取点击的点
    function getPointFromPrimitive(primitiveId) {
      for (let i = 0; i < navigationPointData.length; i++) {
        if (navigationPointData[i].id === primitiveId) {
          return navigationPointData[i]
        }
      }
    }
    // // 显示弹窗
    function showPopup(point) {
      _this.setState({
        targetData: point
      });

    }
    function openModal() {
      _this.setState({
        showModal: true
      });
    };

    function closeModal() {
      _this.setState({
        showModal: false
      });
    };

  };

  // 搜索功能
  handleSearchPlace() {
    // 添加搜索地点功能
    new Cesium.Geocoder({
      container: 'cesiumSearchContainer',
      scene: viewer.scene,
    });
  }
  render() {
    const { showModal, airportImageData } = this.state;
    return (
      <div >
        <div id="cesiumSearchContainer"></div>
        <div id="cesiumContainer" style={{ width: '100%', height: '100%' }}></div>
        {showModal && (
          <div id="popup" >
            <div>
            </div>
            {<ul>
              {Object.keys(airportImageData).map((key) => (
                <li key={key}>
                  <strong>{key}: </strong>
                  {airportImageData[key] || '暂无信息'}
                </li>
              ))}
            </ul>}
          </div>
        )}
      </div>
    );
  }
}
export default EarthComponent;

