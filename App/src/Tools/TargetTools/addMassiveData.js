
/**
 * 创建时间：2023/8/5
 * 功能描述：加载海量图片
 * 存在问题：
 *      1、图片路径无法动态传递
 *      2、意外校验较弱
 */
export class AddMassiveImages {
    constructor(arrayImageData, iamge, color) {
        this.arrayImageData = arrayImageData;
        this.iamge = iamge;
        this.color = color;
    }
    handlerAddImages() {
        const arrayImageData = this.arrayImageData
        if (Array.isArray(arrayImageData)) {
            const length = arrayImageData.length
            try {
                const billboardBatch = new Cesium.BillboardCollection();
                var date1 = new Date().getTime();
                for (let i = 0; i < length; i++) {
                    const lon = arrayImageData[i].longitude_deg;
                    const lat = arrayImageData[i].latitude_deg;
                    billboardBatch.add({
                        position: Cesium.Cartesian3.fromDegrees(lon, lat, 0),
                        image: require('../../Public/Image/AirportImage/一级机场.png'),
                        // image: require('../Public/Image/AirportImage/一级机场.png'),
                        // color: Cesium.Color.fromCssColorString('#0ccd73'),
                        scale: .4,
                    });
                }

                var date2 = new Date().getTime();
                console.log('时间戳', date2 - date1);
                viewer.scene.primitives.add(billboardBatch);

            } catch {
                throw new Error(`AddMassiveImages函数不可用！
            原因：handlerAddImages函数异常`)
            }
        } else {
            throw new Error(`AddMassiveImages函数不可用！
            原因：检测到参数不是一个数组`)
        }
    }
}

/**
 * 创建时间：2023/8/5
 * 功能描述：加载海量点
 * 存在问题：
 *      1、意外校验较弱
 */
export class AddMassivePoints {
    constructor(arrayPointData, color) {
        this.arrayPointData = arrayPointData;
        this.color = color;
    }
    handlerAddPoints() {
        const arrayPointsData = this.arrayPointData;
        if (Array.isArray(arrayPointsData)) {
            const length = arrayPointsData.length;
            try {
                const primitive = new Cesium.PointPrimitiveCollection();
                const primitiveList = [];
                var date1 = new Date().getTime();
                for (let i = 0; i < length; i++) {
                    const primitiveElement = primitive.add({
                        id: arrayPointsData[i].id,
                        position: Cesium.Cartesian3.fromDegrees(
                            arrayPointsData[i].longitude_deg,
                            arrayPointsData[i].latitude_deg,
                            0
                        ),
                        color: new Cesium.Color(
                            Math.random(),
                            Math.random(),
                            Math.random(),
                            1.0
                        ),
                        pixelSize: 12,
                    });
                    primitiveList.push(primitiveElement)
                }
                var date2 = new Date().getTime();
                viewer.scene.primitives.add(primitive);
                return primitiveList
            } catch {
                throw new Error(`AddMassivePoints函数不可用！
               原因：handlerAddPoints函数异常
              `)
            }
        } else {
            throw new Error(`AddMassivePoints函数不可用！
            原因：检测到参数不是一个数组`)
        }
    }
}


