import React from 'react';
import LoginIn from './Page/loginIn'
import { Provider } from 'react-redux';
class App extends React.Component {

  render() {
    
    return (
      <Provider>
          <LoginIn />
      </Provider>

    )
  }
}

export default App;

